package scheduler

import "context"

type Job struct {
	cancel context.CancelFunc
	task   Task
}

type Task func(ctx context.Context)
