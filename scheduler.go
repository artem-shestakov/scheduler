package scheduler

import (
	"context"
	"errors"
	"fmt"
	"sync"
	"time"
)

type Scheduler struct {
	Wg   *sync.WaitGroup
	Jobs map[string]Job
}

func NewScheduler() *Scheduler {
	return &Scheduler{
		Wg:   new(sync.WaitGroup),
		Jobs: make(map[string]Job),
	}
}

func (s *Scheduler) createJob(taskName string, task Task, cancel context.CancelFunc) error {
	_, ok := s.Jobs[taskName]
	if ok {
		fmt.Println("Task with this name already exists")
		return errors.New("Task with this name already exists")
	} else {
		job := Job{
			task:   task,
			cancel: cancel,
		}
		s.Jobs[taskName] = job
	}
	return nil
}

func (s *Scheduler) AddIntervalTask(ctx context.Context, taskName string, task Task, interval time.Duration) error {
	ctx, cancel := context.WithCancel(ctx)
	err := s.createJob(taskName, task, cancel)
	if err != nil {
		return err
	}
	ticker := time.NewTicker(interval * time.Second)
	s.Wg.Add(1)
	go func() {
		for {
			select {
			case <-ticker.C:
				task(ctx)
			case <-ctx.Done():
				s.Wg.Done()
				return
			}
		}
	}()
	return nil
}

func (s *Scheduler) CancelTask(ctx context.Context, taskName string) error {
	if _, ok := s.Jobs[taskName]; ok {
		s.Jobs[taskName].cancel()
		delete(s.Jobs, taskName)
		return nil
	}
	return errors.New("Task is not found")
}

func (s *Scheduler) AddDeadLineTask(ctx context.Context, dt time.Time) {
	d := time.Now().Add(10 * time.Second)
	ctx, cancel := context.WithDeadline(ctx, d)
	s.Wg.Add(1)
	fmt.Println(time.Now())
	go func() {
		defer cancel()
		select {
		case <-time.After(9 * time.Second):
			fmt.Println("Task overslept")
		case <-ctx.Done():
			fmt.Println("done:", time.Now())
			fmt.Println(ctx.Err())
			cancel()
		}
	}()
}
